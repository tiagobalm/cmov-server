package com.feup.cmov.controller;

import com.feup.cmov.domain.Event;
import com.feup.cmov.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EventController {

    @Autowired
    EventService eventService;

    @GetMapping(value = "/events")
    public List<Event> getAllEvents() { return eventService.getAllEvents(); }
}
