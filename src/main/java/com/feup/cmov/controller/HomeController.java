package com.feup.cmov.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping(value = "/healthcheck")
    public ResponseEntity index() {
        return new ResponseEntity(null, HttpStatus.OK);
    }

}