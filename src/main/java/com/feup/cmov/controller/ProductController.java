package com.feup.cmov.controller;

import com.feup.cmov.domain.Product;
import com.feup.cmov.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping(value = "/products")
    public List<Product> getAllProducts() { return productService.getAll(); }
}
