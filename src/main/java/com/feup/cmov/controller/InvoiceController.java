package com.feup.cmov.controller;

import com.feup.cmov.dto.CafeteriaOrderDTO;
import com.feup.cmov.dto.CafeteriaOrderResultDTO;
import com.feup.cmov.dto.CreateOrderDTO;
import com.feup.cmov.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class InvoiceController {

    @Autowired
    InvoiceService invoiceService;

    @PostMapping(value = "invoice/process")
    public CafeteriaOrderResultDTO processOrderInvoice(@RequestBody CreateOrderDTO createOrderDTO) { return invoiceService.processOrderInvoice(createOrderDTO); }

    @GetMapping(value = "/invoice/past/{uuid}")
    public List<CafeteriaOrderResultDTO> getAllPastTransactions(@PathVariable("uuid") UUID uuid) { return invoiceService.getAllPastPurchases(uuid); }
}
