package com.feup.cmov.controller;

import com.feup.cmov.dto.CreateOrderDTO;
import com.feup.cmov.dto.TicketDTO;
import com.feup.cmov.dto.ValidateTicketsDTO;
import com.feup.cmov.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class TicketController {

    @Autowired
    TicketService ticketService;

    @GetMapping(value = "/tickets/notused/{uuid}")
    public List<TicketDTO> getUpComingEvents(@PathVariable("uuid") UUID uuid) { return ticketService.getUpComingEvents(uuid); }

    @GetMapping(value = "tickets/used/{uuid}")
    public List<TicketDTO> getAllUsedTickets(@PathVariable("uuid") UUID uuid) { return ticketService.getAllUsedTickets(uuid); }

    @PostMapping(value = "tickets/process")
    public String processOrderTickets(@RequestBody CreateOrderDTO createOrderDTO) { return ticketService.processOrderTickets(createOrderDTO); }

    @PostMapping(value = "tickets/validate")
    public String validateTickets(@RequestBody ValidateTicketsDTO validateTicketsDTO) { return ticketService.validateTickets(validateTicketsDTO); }
}
