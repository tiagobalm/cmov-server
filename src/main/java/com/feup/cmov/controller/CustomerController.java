package com.feup.cmov.controller;

import com.feup.cmov.domain.Customer;
import com.feup.cmov.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping(value = "/customer/{uuid}")
    public Optional<Customer> getCustomer(@PathVariable("uuid") UUID uuid) { return customerService.findById(uuid); }

    @PostMapping(value = "/customer/create")
    public UUID createCustomer(@RequestBody Customer customerDTO) { return customerService.create(customerDTO); }

}
