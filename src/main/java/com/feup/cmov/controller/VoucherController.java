package com.feup.cmov.controller;

import com.feup.cmov.domain.Customer;
import com.feup.cmov.domain.Voucher;
import com.feup.cmov.service.CustomerService;
import com.feup.cmov.service.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class VoucherController {

    @Autowired
    VoucherService voucherService;

    @GetMapping(value = "/vouchers/unused/{uuid}")
    public List<Voucher> getAllUnusedVouchers(@PathVariable("uuid") UUID uuid) { return voucherService.getAllUnusedVouchers(uuid); }
}
