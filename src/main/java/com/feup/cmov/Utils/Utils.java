package com.feup.cmov.Utils;

public class Utils {
    public static String COFFEE = "COFFEE";
    public static String POPCORN = "POPCORN";
    public static String FIVEPERCENT = "FIVEPERCENT";
    public static Integer KEY_SIZE = 512;
    public static String SIGN_ALGORITHM = "SHA256WithRSA";
    public static String SUCCESS_STRING = "SUCCESS";
}
