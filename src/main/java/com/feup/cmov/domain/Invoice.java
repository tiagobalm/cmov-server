package com.feup.cmov.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "invoice")
public class Invoice {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private Integer uuid;

    @Column(name = "date")
    private Date date;

    @Column(name = "totalprice")
    private Double totalPrice;

    @Column(name = "customeruuid")
    private UUID customerUUID;

    @Column(name = "firstvoucheruuid")
    private UUID firstVoucherUUID;

    @Column(name = "secondvoucheruuid")
    private UUID secondVoucherUUID;

    protected Invoice() {}

    public Invoice(Date date, Double totalPrice, UUID customerUUID, UUID firstVoucherUUID, UUID secondVoucherUUID) {
        this.date = date;
        this.totalPrice = totalPrice;
        this.customerUUID = customerUUID;
        this.firstVoucherUUID = firstVoucherUUID;
        this.secondVoucherUUID = secondVoucherUUID;
    }

    public Integer getUuid() {
        return uuid;
    }

    public void setUuid(Integer uuid) {
        this.uuid = uuid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public UUID getCustomerUUID() {
        return customerUUID;
    }

    public void setCustomerUUID(UUID customerUUID) {
        this.customerUUID = customerUUID;
    }

    public UUID getFirstVoucherUUID() {
        return firstVoucherUUID;
    }

    public void setFirstVoucherUUID(UUID firstVoucherUUID) {
        this.firstVoucherUUID = firstVoucherUUID;
    }

    public UUID getSecondVoucherUUID() {
        return secondVoucherUUID;
    }

    public void setSecondVoucherUUID(UUID secondVoucherUUID) {
        this.secondVoucherUUID = secondVoucherUUID;
    }

    @Override
    public String toString() {
        return String.format("Invoice[id=%d, date='%s', totalprice='%f, customeruuid='%s, firstvoucheruuid='%s, secondvoucheruuid='%s]", uuid, date, totalPrice, customerUUID, firstVoucherUUID, secondVoucherUUID);
    }
}
