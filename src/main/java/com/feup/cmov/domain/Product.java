package com.feup.cmov.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private Integer uuid;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Double price;

    protected Product() {}

    public Product(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public Integer getUuid() {
        return uuid;
    }

    public void setUuid(Integer uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return String.format("Product[id=%d, name='%s', price='%f]", uuid, name, price);
    }
}
