package com.feup.cmov.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    private UUID uuid;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "nif")
    private Long nif;

    @Column(name = "cardtype")
    private String cardType;

    @Column(name = "cardnumber")
    private Long cardNumber;

    @Column(name = "cardvalidity")
    private Date cardValidity;

    @Column(name = "keymodulus")
    private byte[] keyModulus;

    @Column(name = "keyexponent")
    private byte[] keyExponent;

    @Column(name = "voucherratio")
    private Double voucherRatio;

    protected Customer() {}

    public Customer(UUID uuid, String firstName, String lastName, Long nif, String cardType, Long cardNumber, Date cardValidity, byte[] keyModulus, byte[] keyExponent) {
        this.uuid = uuid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nif = nif;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.cardValidity = cardValidity;
        this.keyModulus = keyModulus;
        this.keyExponent = keyExponent;
        this.voucherRatio = 0.0;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getNif() {
        return nif;
    }

    public void setNif(Long nif) {
        this.nif = nif;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public Long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Date getCardValidity() {
        return cardValidity;
    }

    public void setCardValidity(Date cardValidity) {
        this.cardValidity = cardValidity;
    }

    public byte[] getKeyModulus() {
        return keyModulus;
    }

    public void setKeyModulus(byte[] keyModulus) {
        this.keyModulus = keyModulus;
    }

    public byte[] getKeyExponent() {
        return keyExponent;
    }

    public void setKeyExponent(byte[] keyExponent) {
        this.keyExponent = keyExponent;
    }

    public Double getVoucherRatio() {
        return voucherRatio;
    }

    public void setVoucherRatio(Double voucherRatio) {
        this.voucherRatio = voucherRatio;
    }
}