package com.feup.cmov.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    private UUID uuid;

    @Column(name = "customeruuid")
    private UUID customerUUID;

    @Column(name = "seat")
    private Integer seat;

    @Column(name = "used")
    private Boolean used;

    @Column(name = "dateofpurchase")
    private Date dateOfPurchase;

    @Column(name = "eventuuid")
    private Integer eventUUID;

    protected Ticket() {}

    public Ticket(UUID uuid, UUID customerUUID, Integer seat, Boolean used, Date dateOfPurchase, Integer eventUUID) {
        this.uuid = uuid;
        this.customerUUID = customerUUID;
        this.seat = seat;
        this.used = used;
        this.dateOfPurchase = dateOfPurchase;
        this.eventUUID = eventUUID;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUserUUID() {
        return customerUUID;
    }

    public void setUserUUID(UUID userUUID) {
        this.customerUUID = userUUID;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    public Date getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(Date dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public Integer getEventUUID() {
        return eventUUID;
    }

    public void setEventUUID(Integer eventUUID) {
        this.eventUUID = eventUUID;
    }

    @Override
    public String toString() {
        return String.format("Ticker[uuid='%s', seat='%d', used='%b', dateofpurchase='%s', eventuuid='%d']", uuid, seat, used, dateOfPurchase, eventUUID);
    }
}
