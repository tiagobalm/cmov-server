package com.feup.cmov.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "productinvoice")
public class ProductInvoice implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private Integer productInvoiceID;

    @Column(name = "invoiceUUID")
    private Integer invoiceUUID;

    @Column(name = "productuuid")
    private Integer productUUID;

    @Column(name = "numberofproducts")
    private Integer numberOfProducts;

    protected ProductInvoice() {}

    public ProductInvoice(Integer invoiceUUID, Integer productUUID, Integer numberOfProducts) {
        this.invoiceUUID = invoiceUUID;
        this.productUUID = productUUID;
        this.numberOfProducts = numberOfProducts;
    }

    public Integer getProductInvoiceID() {
        return productInvoiceID;
    }

    public void setProductInvoiceID(Integer productInvoiceID) {
        this.productInvoiceID = productInvoiceID;
    }

    public Integer getOrderUUID() {
        return invoiceUUID;
    }

    public void setOrderUUID(Integer orderUUID) {
        this.invoiceUUID = orderUUID;
    }

    public Integer getProductUUID() {
        return productUUID;
    }

    public void setProductUUID(Integer productUUID) {
        this.productUUID = productUUID;
    }

    public Integer getInvoiceUUID() {
        return invoiceUUID;
    }

    public void setInvoiceUUID(Integer invoiceUUID) {
        this.invoiceUUID = invoiceUUID;
    }

    public Integer getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(Integer numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    @Override
    public String toString() {
        return String.format("Product Order[orderuuid='%d', productuuid='%d']", invoiceUUID, productUUID);
    }
}
