package com.feup.cmov.domain;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "voucher")
public class Voucher {

    @Id
    private UUID uuid;

    @Column(name = "productcode")
    private String productCode;

    @Column(name = "used")
    private Boolean used;

    @Column(name = "customeruuid")
    private UUID customerUUID;

    protected Voucher() {}

    public Voucher(UUID uuid, String productCode, Boolean used, UUID customerUUID) {
        this.uuid = uuid;
        this.productCode = productCode;
        this.used = used;
        this.customerUUID = customerUUID;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public UUID getCustomerUUID() {
        return customerUUID;
    }

    public void setCustomerUUID(UUID customerUUID) {
        this.customerUUID = customerUUID;
    }

    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

}