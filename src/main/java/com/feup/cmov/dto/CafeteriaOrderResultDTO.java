package com.feup.cmov.dto;

import com.feup.cmov.domain.Product;
import com.feup.cmov.domain.Voucher;

import java.io.Serializable;
import java.util.List;

public class CafeteriaOrderResultDTO implements Serializable {
    private String result;
    private List<Product> products;
    private List<Integer> numberOfProducts;
    private Double totalPrice;
    List<Voucher> vouchers;

    public CafeteriaOrderResultDTO() {}

    public CafeteriaOrderResultDTO(String result, List<Product> products, List<Integer> numberOfProducts, Double totalPrice, List<Voucher> vouchers) {
        this.result = result;
        this.products = products;
        this.numberOfProducts = numberOfProducts;
        this.totalPrice = totalPrice;
        this.vouchers = vouchers;
        this.totalPrice = totalPrice;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Integer> getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(List<Integer> numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<Voucher> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<Voucher> vouchers) {
        this.vouchers = vouchers;
    }
}
