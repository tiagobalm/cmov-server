package com.feup.cmov.service;

import com.feup.cmov.Utils.DateDeserializer;
import com.feup.cmov.Utils.Utils;
import com.feup.cmov.domain.*;
import com.feup.cmov.dto.CafeteriaOrderDTO;
import com.feup.cmov.dto.CafeteriaOrderResultDTO;
import com.feup.cmov.dto.CreateOrderDTO;
import com.feup.cmov.repository.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
public class InvoiceService {

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    ProductInvoiceRepository productInvoiceRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    VoucherRepository voucherRepository;

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    CustomerRepository customerRepository;

    public CafeteriaOrderResultDTO processOrderInvoice(CreateOrderDTO createOrderDTO) {

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        Gson gson = gsonBuilder.create();

        try {
            CafeteriaOrderDTO sentData = gson.fromJson(new String(createOrderDTO.getData(), UTF_8), CafeteriaOrderDTO.class);
            UUID customerUUID = sentData.getCustomerUUID();
            Customer customer = customerRepository.getOne(customerUUID);

            Signature sig = Signature.getInstance(Utils.SIGN_ALGORITHM);

            RSAPublicKeySpec RSASpec = new RSAPublicKeySpec(new BigInteger(customer.getKeyModulus()), new BigInteger(customer.getKeyExponent()));
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = factory.generatePublic(RSASpec);

            sig.initVerify(publicKey);
            sig.update(createOrderDTO.getData());

            if(sig.verify(createOrderDTO.getSignedData())) {

                List<Integer> products = sentData.getProducts();
                List<Integer> numberOfProducts = sentData.getNumberProducts();

                double totalPrice = 0.0;
                List<Product> productsToReturn = new ArrayList<>();
                List<Voucher> vouchersToReturn = new ArrayList<>();
                boolean usedFirstVoucher = false, usedSecondVoucher = false;

                UUID firstVoucherUUID = sentData.getVouchers().get(0);
                if(firstVoucherUUID == null)
                    vouchersToReturn.add(null);
                else
                    vouchersToReturn.add(voucherRepository.getOne(firstVoucherUUID));

                UUID secondVoucherUUID = sentData.getVouchers().get(1);
                if(secondVoucherUUID == null)
                    vouchersToReturn.add(null);
                else
                    vouchersToReturn.add(voucherRepository.getOne(secondVoucherUUID));

                invoiceRepository.save(new Invoice(new Date(), 0.0, customerUUID, firstVoucherUUID, secondVoucherUUID));
                Integer lastInvoiceID = invoiceRepository.getLastInvoiceIDByCustomer(customerUUID);

                for(int i = 0; i < products.size(); i++) {

                    boolean usedVoucherInThisPurchase = false;
                    Product product = productRepository.getOne(products.get(i));
                    productsToReturn.add(product);

                    totalPrice += product.getPrice() * numberOfProducts.get(i);

                    if(!usedFirstVoucher && vouchersToReturn.get(0) != null) {
                        if(product.getName().toLowerCase().contains(vouchersToReturn.get(0).getProductCode().toLowerCase())) {
                            totalPrice -= product.getPrice();
                            usedFirstVoucher = true;
                            usedVoucherInThisPurchase = true;
                        }
                    }

                    if(!usedSecondVoucher && vouchersToReturn.get(1) != null) {
                        if(product.getName().toLowerCase().contains(vouchersToReturn.get(1).getProductCode().toLowerCase())
                            && (!usedVoucherInThisPurchase || numberOfProducts.get(i) > 1)) {
                            totalPrice -= product.getPrice();
                            usedSecondVoucher = true;
                        }
                    }

                    productInvoiceRepository.save(new ProductInvoice(lastInvoiceID, products.get(i), numberOfProducts.get(i)));
                }

                if(!usedFirstVoucher && vouchersToReturn.get(0).getProductCode().equals(Utils.FIVEPERCENT)) {
                    totalPrice -= totalPrice * 0.05;
                    usedFirstVoucher = true;
                } else if(!usedSecondVoucher && vouchersToReturn.get(1).getProductCode().equals(Utils.FIVEPERCENT)) {
                    totalPrice -= totalPrice * 0.05;
                    usedSecondVoucher = true;
                }

                if(usedFirstVoucher) {
                    Voucher voucher = vouchersToReturn.get(0);
                    voucher.setUsed(true);
                    voucherRepository.save(voucher);
                }

                if(usedSecondVoucher) {
                    Voucher voucher = vouchersToReturn.get(1);
                    voucher.setUsed(true);
                    voucherRepository.save(voucher);
                }

                Invoice invoice = invoiceRepository.getOne(lastInvoiceID);

                if(!usedSecondVoucher) {
                    vouchersToReturn.remove(1);
                    invoice.setSecondVoucherUUID(null);
                }

                if(!usedFirstVoucher) {
                    vouchersToReturn.remove(0);
                    invoice.setFirstVoucherUUID(null);
                }

                invoice.setTotalPrice(totalPrice);
                invoiceRepository.save(invoice);

                return new CafeteriaOrderResultDTO("Success", productsToReturn, numberOfProducts, totalPrice, vouchersToReturn);
            }

        } catch (NoSuchAlgorithmException | SignatureException | InvalidKeyException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return new CafeteriaOrderResultDTO("Error", null, null, 0.0, null);
    }

    public List<CafeteriaOrderResultDTO> getAllPastPurchases(UUID uuid) {

        List<CafeteriaOrderResultDTO> cafeteriaOrderResultDTOS = new ArrayList<>();
        List<Invoice> invoices = invoiceRepository.getAllPastInvoicesByCustomer(uuid);

        for(Invoice invoice : invoices) {
            List<ProductInvoice> productInvoices = productInvoiceRepository.getAllProductsByInvoiceUUID(invoice.getUuid());
            List<Product> sentProducts = new ArrayList<>();
            List<Integer> sentNumberProducts = new ArrayList<>();
            List<Voucher> usedVouchers = new ArrayList<>();
            double totalPrice = 0;

            if(invoice.getFirstVoucherUUID() != null && voucherRepository.findById(invoice.getFirstVoucherUUID()).isPresent()) {
                Voucher voucher = voucherRepository.findById(invoice.getFirstVoucherUUID()).get();
                if(voucher.getUsed())
                    usedVouchers.add(voucher);
            }

            if(invoice.getSecondVoucherUUID() != null && voucherRepository.findById(invoice.getSecondVoucherUUID()).isPresent()) {
                Voucher voucher = voucherRepository.findById(invoice.getSecondVoucherUUID()).get();
                if(voucher.getUsed())
                    usedVouchers.add(voucher);
            }

            for(ProductInvoice productInvoice : productInvoices) {
                if(productRepository.findById(productInvoice.getProductUUID()).isPresent()) {
                    sentProducts.add(productRepository.findById(productInvoice.getProductUUID()).get());
                    sentNumberProducts.add(productInvoice.getNumberOfProducts());
                    totalPrice += sentProducts.get(sentProducts.size() - 1).getPrice() * productInvoice.getNumberOfProducts();
                }
            }

            cafeteriaOrderResultDTOS.add(new CafeteriaOrderResultDTO("", sentProducts, sentNumberProducts, totalPrice, usedVouchers));
        }

        return cafeteriaOrderResultDTOS;
    }
}
