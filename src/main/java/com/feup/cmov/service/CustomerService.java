package com.feup.cmov.service;

import com.feup.cmov.domain.Customer;
import com.feup.cmov.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    public Optional<Customer> findById(UUID uuid) { return customerRepository.findById(uuid); }

    public List<Customer> getAll() { return customerRepository.findAll(); }

    public UUID create(Customer customer) {
        UUID uuid = UUID.randomUUID();

        while(customerRepository.findById(uuid).isPresent())
            uuid = UUID.randomUUID();

        customer.setUuid(uuid);
        customerRepository.save(customer);

        return uuid;
    }

    public ResponseEntity update(Customer customerDTO) {
        Optional<Customer> temporaryCustomer = customerRepository.findById(customerDTO.getUuid());

        if(!temporaryCustomer.isPresent())
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);

        Customer customer = temporaryCustomer.get();

        customer.setFirstName(customerDTO.getFirstName());
        customer.setLastName(customerDTO.getLastName());
        customer.setNif(customerDTO.getNif());
        customer.setCardType(customerDTO.getCardType());
        customer.setCardNumber(customerDTO.getCardNumber());
        customer.setCardValidity(customerDTO.getCardValidity());
        customerRepository.save(customer);

        return new ResponseEntity(null, HttpStatus.OK);
    }

    public ResponseEntity delete(UUID uuid) {
        Optional<Customer> temporaryCustomer = customerRepository.findById(uuid);

        if(!temporaryCustomer.isPresent())
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);

        customerRepository.deleteById(uuid);
        return new ResponseEntity(null, HttpStatus.OK);
    }
}
