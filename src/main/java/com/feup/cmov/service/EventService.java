package com.feup.cmov.service;

import com.feup.cmov.domain.Event;
import com.feup.cmov.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventService {

    @Autowired
    EventRepository eventRepository;

    public List<Event> getAllEvents() { return eventRepository.getAllEventsOrdered(); }
}
