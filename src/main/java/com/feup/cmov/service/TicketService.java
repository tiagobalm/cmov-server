package com.feup.cmov.service;

import com.feup.cmov.Utils.DateDeserializer;
import com.feup.cmov.Utils.Utils;
import com.feup.cmov.domain.Customer;
import com.feup.cmov.domain.Event;
import com.feup.cmov.domain.Ticket;
import com.feup.cmov.domain.Voucher;
import com.feup.cmov.dto.CreateOrderDTO;
import com.feup.cmov.dto.TicketDTO;
import com.feup.cmov.dto.ValidateTicketsDTO;
import com.feup.cmov.repository.CustomerRepository;
import com.feup.cmov.repository.EventRepository;
import com.feup.cmov.repository.TicketRepository;
import com.feup.cmov.repository.VoucherRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
public class TicketService {

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    VoucherRepository voucherRepository;

    @Autowired
    EventRepository eventRepository;

    public List<TicketDTO> getUpComingEvents(UUID uuid) {

        List<TicketDTO> unusedTickets = new ArrayList<>();
        List<Ticket> tickets = ticketRepository.getUpComingEvents(uuid);

        for(Ticket ticket : tickets)
            unusedTickets.add(new TicketDTO(eventRepository.getOne(ticket.getEventUUID()), ticket));

        unusedTickets.sort(Comparator.comparing(o -> o.getEvent().getDate()));

        return unusedTickets;
    }

    public List<TicketDTO> getAllUsedTickets(UUID uuid) {

        List<TicketDTO> unusedTickets = new ArrayList<>();
        List<Ticket> tickets = ticketRepository.getAllUsedTickets(uuid);

        for(Ticket ticket : tickets)
            unusedTickets.add(new TicketDTO(eventRepository.getOne(ticket.getEventUUID()), ticket));

        unusedTickets.sort(Comparator.comparing(o -> o.getEvent().getDate()));

        return unusedTickets;
    }

    public String processOrderTickets(CreateOrderDTO createOrderDTO) {

        try {

            JSONArray sentData = new JSONArray(new String(createOrderDTO.getData(), UTF_8));
            UUID customerUUID = UUID.fromString(sentData.getString(0));
            Customer customer = customerRepository.getOne(customerUUID);

            Signature sig = Signature.getInstance(Utils.SIGN_ALGORITHM);

            RSAPublicKeySpec RSASpec = new RSAPublicKeySpec(new BigInteger(customer.getKeyModulus()), new BigInteger(customer.getKeyExponent()));
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = factory.generatePublic(RSASpec);

            sig.initVerify(publicKey);
            sig.update(createOrderDTO.getData());

            if(sig.verify(createOrderDTO.getSignedData())) {

                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
                Gson gson = gsonBuilder.create();

                Type eventListType = new TypeToken<ArrayList<Event>>(){}.getType();
                ArrayList<Event> events = gson.fromJson(sentData.getJSONObject(1).getString("events"), eventListType);

                Type numberOfTicketsListType = new TypeToken<ArrayList<Integer>>(){}.getType();
                ArrayList<Integer> numberOfTickets = gson.fromJson(sentData.getJSONObject(2).getString("numberOfTickets"), numberOfTicketsListType);

                for(int i = 0; i < events.size(); i++) {
                    for(int j = 0; j < numberOfTickets.get(i); j++) {

                        UUID uuid = UUID.randomUUID();

                        while(ticketRepository.findById(uuid).isPresent())
                            uuid = UUID.randomUUID();

                        ticketRepository.save(new Ticket(uuid, customerUUID, ThreadLocalRandom.current().nextInt(1, 300 + 1), false, new Date(), events.get(i).getUuid()));

                        int randomVoucher = ThreadLocalRandom.current().nextInt(1, 2 + 1);

                        UUID voucherUUID = UUID.randomUUID();
                        while(voucherRepository.findById(voucherUUID).isPresent())
                            voucherUUID = UUID.randomUUID();

                        if(randomVoucher == 1) voucherRepository.save(new Voucher(voucherUUID, Utils.COFFEE, false, customerUUID));
                        else voucherRepository.save(new Voucher(voucherUUID, Utils.POPCORN, false, customerUUID));

                    }

                    double newVoucherRatio = customer.getVoucherRatio() + events.get(i).getPrice() * numberOfTickets.get(i);
                    double numberOfVouchers = Math.floor(newVoucherRatio / 100);
                    customer.setVoucherRatio(newVoucherRatio - 100 * numberOfVouchers);

                    for(int j = 0; j < numberOfVouchers; j++) {
                        UUID voucherUUID = UUID.randomUUID();

                        while(voucherRepository.findById(voucherUUID).isPresent())
                            voucherUUID = UUID.randomUUID();

                        voucherRepository.save(new Voucher(voucherUUID, Utils.FIVEPERCENT, false, customerUUID));
                    }
                }

                customerRepository.save(customer);
            }

        } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | SignatureException e) {
            e.printStackTrace();
        }

        return Utils.SUCCESS_STRING;
    }

    public String validateTickets(ValidateTicketsDTO validateTicketsDTO) {

        //Changed to Java 8 expressions, if it doesn't work that's why
        List<UUID> ticketsUUID = validateTicketsDTO.getTickets();

        List<Ticket> tickets = ticketRepository.findAllById(ticketsUUID);
        Integer eventUUID = tickets.get(0).getEventUUID();

        if(tickets.stream().allMatch(ticket -> ticket.getEventUUID().equals(eventUUID))) {
            for(Ticket ticket : tickets) {
                ticket.setUsed(true);
                ticketRepository.save(ticket);
            }
        } else
            return "Please only validate tickets from the same event.";

        return "Success";
    }
}
