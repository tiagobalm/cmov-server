package com.feup.cmov.service;

import com.feup.cmov.domain.Customer;
import com.feup.cmov.domain.Voucher;
import com.feup.cmov.repository.CustomerRepository;
import com.feup.cmov.repository.VoucherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class VoucherService {

    @Autowired
    VoucherRepository voucherRepository;

    public List<Voucher> getAllUnusedVouchers(UUID uuid) { return voucherRepository.getAllUnusedVouchers(uuid); }
}
