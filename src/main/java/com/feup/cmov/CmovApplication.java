package com.feup.cmov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CmovApplication {

    public static void main(String[] args) {
        SpringApplication.run(CmovApplication.class, args);
    }
}
