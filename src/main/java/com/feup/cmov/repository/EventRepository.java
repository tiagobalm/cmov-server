package com.feup.cmov.repository;

import com.feup.cmov.domain.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EventRepository extends JpaRepository<Event, Integer> {

    @Query(value = "SELECT * FROM event ORDER BY date ASC",  nativeQuery = true)
    List<Event> getAllEventsOrdered();

}
