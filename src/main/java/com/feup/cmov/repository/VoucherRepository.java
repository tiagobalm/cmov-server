package com.feup.cmov.repository;

import com.feup.cmov.domain.Customer;
import com.feup.cmov.domain.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface VoucherRepository extends JpaRepository<Voucher, UUID> {

    @Query(value = "SELECT * FROM voucher WHERE customeruuid = ?1 AND used = false", nativeQuery = true)
    List<Voucher> getAllUnusedVouchers(UUID uuid);
}
