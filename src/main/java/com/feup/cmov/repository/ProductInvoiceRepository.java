package com.feup.cmov.repository;

import com.feup.cmov.domain.Customer;
import com.feup.cmov.domain.ProductInvoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProductInvoiceRepository extends JpaRepository<ProductInvoice, Integer> {

    @Query(value = "SELECT * FROM productinvoice WHERE invoiceUUID = ?1", nativeQuery = true)
    List<ProductInvoice> getAllProductsByInvoiceUUID(Integer uuid);
}
