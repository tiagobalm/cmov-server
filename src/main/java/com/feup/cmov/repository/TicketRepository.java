package com.feup.cmov.repository;

import com.feup.cmov.domain.Customer;
import com.feup.cmov.domain.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface TicketRepository extends JpaRepository<Ticket, UUID> {

    @Query(value = "SELECT * FROM ticket WHERE customeruuid = ?1 AND used = false", nativeQuery = true)
    List<Ticket> getUpComingEvents(UUID uuid);

    @Query(value = "SELECT * FROM ticket WHERE customeruuid = ?1 AND used = true", nativeQuery = true)
    List<Ticket> getAllUsedTickets(UUID uuid);
}
