package com.feup.cmov.repository;

import com.feup.cmov.domain.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {

    @Query(value = "SELECT * FROM invoice WHERE customeruuid = ?1", nativeQuery = true)
    List<Invoice> getAllPastInvoicesByCustomer(UUID uuid);

    @Query(value = "SELECT MAX(uuid) from invoice WHERE customerUUID = ?1", nativeQuery = true)
    Integer getLastInvoiceIDByCustomer(UUID uuid);
}
